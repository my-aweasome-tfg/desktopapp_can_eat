package com.dam.tfg.can_eat.main;



import com.dam.tfg.can_eat.hibernate.controller.HibernateSession;
import com.dam.tfg.can_eat.hibernate.modelo.Alergeno;
import com.dam.tfg.can_eat.hibernate.modelo.Marca;
import com.dam.tfg.can_eat.hibernate.modelo.Producto;
import com.dam.tfg.can_eat.jdbc.Conector;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
	//Guarda el nombre del Pane en el que estoy para las opciones (añadir, modificar)
	public static String NAME_PANE = "ALERGENO";
	
	public static HibernateSession BD_SESSION;
	public static Conector JDBC_SESSION;
	//Auxiliar para transferir objeto entre views
	public static Alergeno ALERGENO_SELECTED;
	public static Marca MARCA_SELECTED;
	public static Producto PRODUTO_SELECTED;
	//Max_id
	public static int ALERGENO_MAX_ID = 0;
	public static int MARCA_MAX_ID = 0;
	public static int PRODUTO_MAX_ID = 0;
	

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("/view/LoginView.fxml"));
		Scene scene = new Scene(root);
		
		//...
		primaryStage.getIcons().add(new Image("/img/icono.png"));
		primaryStage.setTitle("Gestión: Can I eat It ");
		
		//TODO Eliminar Windows Bar y añadir toolbar personalizada
		//primaryStage.initStyle(StageStyle.TRANSPARENT);
		
		//Bloquear maximizar
		primaryStage.resizableProperty().setValue(false);
		//...
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}

	
	public static void main(String[] args)  {

		launch(args);

	}

}
