package com.dam.tfg.can_eat.controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.dam.tfg.can_eat.ftp.FTPUtil;
import com.dam.tfg.can_eat.hibernate.modelo.Alergeno;
import com.dam.tfg.can_eat.hibernate.modelo.Marca;
import com.dam.tfg.can_eat.hibernate.modelo.Producto;
import com.dam.tfg.can_eat.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class menuController implements Initializable {

	// PaneProductos
	@FXML
	private Pane paneProductos;

	@FXML
	private TableView<Producto> ListProductos;

	@FXML
	private TableColumn<Producto, Integer> columnIdProducto;

	@FXML
	private TableColumn<Producto, String> columnNombreProducto;

	@FXML
	private TableColumn<Producto, String> columnDescripcionProducto;

	@FXML
	private TextField textBuscadorProductos;

	@FXML
	void onActionAñadirProductos(ActionEvent event) throws IOException {
		showStage("/view/AñadirView.fxml");
	}

	@FXML
	void onActionBuscarProductos(ActionEvent event) {
		String aux = textBuscadorProductos.getText();
		loadTableProductos(aux);
	}

	@FXML
	void onActionEliminarProductos(ActionEvent event) {
		Main.BD_SESSION.deleteRow(Main.PRODUTO_SELECTED);
		loadTableProductos();

	}

	@FXML
	void onActionModificarProductos(ActionEvent event) {

	}

	@FXML
	void accionProducto(ActionEvent event) {
		Main.NAME_PANE = "PRODUCTO";
		paneProductos.toFront();
	}

	// Pane Marcas
	@FXML
	private Pane paneMarcas;

	@FXML
	private TableView<Marca> ListMarcas;

	@FXML
	private TableColumn<Marca, Integer> columnIdMarca;

	@FXML
	private TableColumn<Marca, String> columnNombreMarca;

	@FXML
	private TableColumn<Marca, String> columnDescripcionMarca;

	@FXML
	private TextField textBuscadorMarcas;

	@FXML
	void onActionAñadirMarcas(ActionEvent event) throws IOException {
		showStage("/view/AñadirView.fxml");
	}

	@FXML
	void onActionBuscarMarcas(ActionEvent event) {
		String aux = textBuscadorMarcas.getText();
		loadTableMarcas(aux);
	}

	@FXML
	void onActionEliminarMarcas(ActionEvent event) {
		Main.BD_SESSION.deleteRow(Main.MARCA_SELECTED);
		loadTableMarcas();

	}

	@FXML
	void onActionModificarMarcas(ActionEvent event) {

	}

	@FXML
	void actionMarca(ActionEvent event) {
		Main.NAME_PANE = "MARCA";
		paneMarcas.toFront();
	}

	// Pane Alergeno
	@FXML
	private Pane paneAlergeno;

	@FXML
	private TableView<Alergeno> ListAlergenos;

	@FXML
	private TableColumn<Alergeno, Integer> columnIdAlergeno;

	@FXML
	private TableColumn<Alergeno, String> columnNombreAlergeno;

	@FXML
	private TableColumn<Alergeno, String> columnDescripcionAlergeno;

	@FXML
	private TextField textBuscadorAlergenos;

	@FXML
	void onActionAñadirAlergenos(ActionEvent event) throws IOException {
		showStage("/view/AñadirView.fxml");

	}

	@FXML
	void onActionBuscarAlergenos(ActionEvent event) {
		String aux = textBuscadorAlergenos.getText();
		loadTableAlergenos(aux);

	}

	@FXML
	void onActionEliminarAlergenos(ActionEvent event) {
		Main.BD_SESSION.deleteRow(Main.ALERGENO_SELECTED);
		loadTableAlergenos();
	}

	@FXML
	void onActionModificarAlergenos(ActionEvent event) {

	}

	@FXML
	void accionAlergeno(ActionEvent event) {
		Main.NAME_PANE = "ALERGENO";
		paneAlergeno.toFront();
	}

	// Logout
	@FXML
	void accionLogout(ActionEvent event) throws IOException {
		FTPUtil.sendFile();
		Main.BD_SESSION.Close();
		changeToLogin(event);

	}

	private void changeToLogin(ActionEvent event) throws IOException {
		Parent blah = FXMLLoader.load(getClass().getResource("/view/LoginView.fxml"));
		Scene scene = new Scene(blah);
		Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		appStage.setScene(scene);
		appStage.show();
	}

	@FXML
	void onMouseClickedAlergeno(MouseEvent event) throws IOException {
		// Obtener item seleccionado
		Alergeno alergenoSelected = ListAlergenos.getSelectionModel().getSelectedItem();
		Main.ALERGENO_SELECTED = alergenoSelected;
		// comprobar doble click
		if (event.getClickCount() == 2) {
			showStage("/view/Element2View.fxml");
			event.consume();
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loadTableAlergenos();
		loadTableProductos();
		loadTableMarcas();

	}

	// Metodos
	private void loadTableAlergenos() {
		// TODO añadir JOptionPane "Pensando"
		List<Alergeno> alergenos = Main.BD_SESSION.getAlergeno();
		ObservableList<Alergeno> obsAlerg = FXCollections.observableArrayList();
		Main.ALERGENO_MAX_ID = 0;
		for (Alergeno a : alergenos) {
			obsAlerg.add(a);
			if (a.getIdAlergeno() > Main.ALERGENO_MAX_ID) {
				Main.ALERGENO_MAX_ID = a.getIdAlergeno();
			}
			// System.out.println(a.toString());
		}
		// Selecionr getter para la cada columna
		columnIdAlergeno.setCellValueFactory(new PropertyValueFactory<>("IdAlergeno"));
		columnNombreAlergeno.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
		columnDescripcionAlergeno.setCellValueFactory(new PropertyValueFactory<>("Descripcion"));

		// aplicar la ObservableList en la tableview
		ListAlergenos.setItems(obsAlerg);

	}

	private void loadTableAlergenos(String aux) {
		// TODO añadir JOptionPane "Pensando"
		List<Alergeno> alergenos = Main.BD_SESSION.getAlergeno();
		ObservableList<Alergeno> obsAlerg = FXCollections.observableArrayList();
		Main.ALERGENO_MAX_ID = 0;
		for (Alergeno a : alergenos) {
			if (a.getNombre().contains(aux)) {
				obsAlerg.add(a);
			}

			if (a.getIdAlergeno() > Main.ALERGENO_MAX_ID) {
				Main.ALERGENO_MAX_ID = a.getIdAlergeno();
			}
			// System.out.println(a.toString());
		}
		// Selecionr getter para la cada columna
		columnIdAlergeno.setCellValueFactory(new PropertyValueFactory<>("IdAlergeno"));
		columnNombreAlergeno.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
		columnDescripcionAlergeno.setCellValueFactory(new PropertyValueFactory<>("Descripcion"));

		// aplicar la ObservableList en la tableview
		ListAlergenos.setItems(obsAlerg);

	}

	private void loadTableProductos() {
		// TODO añadir JOptionPane "Pensando"
		List<Producto> productos = Main.BD_SESSION.getProducto();
		ObservableList<Producto> obsProduct = FXCollections.observableArrayList();

		Main.PRODUTO_MAX_ID = 0;
		for (Producto a : productos) {
			obsProduct.add(a);
			if (a.getIdProducto() > Main.PRODUTO_MAX_ID) {
				Main.PRODUTO_MAX_ID = a.getIdProducto();
			}
			// System.out.println(a.toString());
		}
		// Selecionr getter para la cada columna
		columnIdProducto.setCellValueFactory(new PropertyValueFactory<>("IdProducto"));
		columnNombreProducto.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
		columnDescripcionProducto.setCellValueFactory(new PropertyValueFactory<>("Descripcion"));

		// aplicar la ObservableList en la tableview
		ListProductos.setItems(obsProduct);

	}

	private void loadTableProductos(String aux) {
		// TODO añadir JOptionPane "Pensando"
		List<Producto> productos = Main.BD_SESSION.getProducto();
		ObservableList<Producto> obsProduct = FXCollections.observableArrayList();

		Main.PRODUTO_MAX_ID = 0;
		for (Producto a : productos) {
			if (a.getNombre().contains(aux)) {
				obsProduct.add(a);
			}

			if (a.getIdProducto() > Main.PRODUTO_MAX_ID) {
				Main.PRODUTO_MAX_ID = a.getIdProducto();
			}
			// System.out.println(a.toString());
		}
		// Selecionr getter para la cada columna
		columnIdProducto.setCellValueFactory(new PropertyValueFactory<>("IdProducto"));
		columnNombreProducto.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
		columnDescripcionProducto.setCellValueFactory(new PropertyValueFactory<>("Descripcion"));

		// aplicar la ObservableList en la tableview
		ListProductos.setItems(obsProduct);

	}

	private void loadTableMarcas() {
		// TODO añadir JOptionPane "Pensando"
		List<Marca> marcas = Main.BD_SESSION.getMarca();
		ObservableList<Marca> obsMarca = FXCollections.observableArrayList();

		Main.MARCA_MAX_ID = 0;
		for (Marca a : marcas) {
			obsMarca.add(a);
			if (a.getIdMarca() > Main.MARCA_MAX_ID) {
				Main.MARCA_MAX_ID = a.getIdMarca();
			}
			// System.out.println(a.toString());
		}
		// Selecionr getter para la cada columna
		columnIdMarca.setCellValueFactory(new PropertyValueFactory<>("IdMarca"));
		columnNombreMarca.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
		columnDescripcionMarca.setCellValueFactory(new PropertyValueFactory<>("Descripcion"));

		// aplicar la ObservableList en la tableview
		ListMarcas.setItems(obsMarca);

	}

	private void loadTableMarcas(String aux) {
		// TODO añadir JOptionPane "Pensando"
		List<Marca> marcas = Main.BD_SESSION.getMarca();
		ObservableList<Marca> obsMarca = FXCollections.observableArrayList();

		Main.MARCA_MAX_ID = 0;
		for (Marca a : marcas) {
			if (a.getNombre().contains(aux)) {
				obsMarca.add(a);
			}

			if (a.getIdMarca() > Main.MARCA_MAX_ID) {
				Main.MARCA_MAX_ID = a.getIdMarca();
			}
			// System.out.println(a.toString());
		}
		// Selecionr getter para la cada columna
		columnIdMarca.setCellValueFactory(new PropertyValueFactory<>("IdMarca"));
		columnNombreMarca.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
		columnDescripcionMarca.setCellValueFactory(new PropertyValueFactory<>("Descripcion"));

		// aplicar la ObservableList en la tableview
		ListMarcas.setItems(obsMarca);

	}

	private void showStage(String view) throws IOException {
		// Abrir nuevo stage
		Parent newRoot = FXMLLoader.load(getClass().getResource(view));
		Stage stage2 = new Stage();
		stage2.setScene(new Scene(newRoot));
		// Holdear Stage principal
		stage2.getIcons().add(new Image("/img/icono.png"));
		stage2.initOwner(paneAlergeno.getScene().getWindow());
		stage2.initModality(Modality.WINDOW_MODAL);
		stage2.resizableProperty().setValue(false);

		stage2.show();
	}

	@FXML
	void onMouseClickedMarcas(MouseEvent event) throws IOException {
		System.out.println(1);
		// Obtener item seleccionado
		Marca marcaSelected = ListMarcas.getSelectionModel().getSelectedItem();
		Main.MARCA_SELECTED = marcaSelected;
		System.out.println(marcaSelected.toString());

		// comprobar doble click
		if (event.getClickCount() == 2) {
			showStage("/view/Element2View.fxml");
			event.consume();
		}

	}

	@FXML
	void onMouseClickedProductos(MouseEvent event) throws IOException {

		// Obtener item seleccionado
		Producto productoSelected = ListProductos.getSelectionModel().getSelectedItem();
		Main.PRODUTO_SELECTED = productoSelected;
		System.out.println(productoSelected.toString());

		// comprobar doble click
		if (event.getClickCount() == 2) {
			showStage("/view/ElementView.fxml");

			event.consume();
		}

	}

	@FXML
	void onActionReloadAlergenos(ActionEvent event) {
		loadTableAlergenos();
	}

	@FXML
	void onActionReloadMarcas(ActionEvent event) {
		loadTableMarcas();
	}

	@FXML
	void onActionReloadProductos(ActionEvent event) {
		loadTableProductos();
	}

}