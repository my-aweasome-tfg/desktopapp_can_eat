package com.dam.tfg.can_eat.controller;

import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.swing.plaf.metal.MetalPopupMenuSeparatorUI;

import com.dam.tfg.can_eat.hibernate.modelo.Alergeno;
import com.dam.tfg.can_eat.hibernate.modelo.Marca;
import com.dam.tfg.can_eat.hibernate.modelo.Producto;
import com.dam.tfg.can_eat.main.Main;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;

public class añadirController implements Initializable {
	private Alergeno alergenoContener = null;
	private Alergeno alergenoNoContener = null;

	@FXML
	private Button buttonAdd;

	@FXML
	private Button buttonRemove;

	@FXML
	private Text paneTitle;

	@FXML
	private Text labelMarca;

	@FXML
	private Text labelAlergenos;

	@FXML
	private TableView<Alergeno> tableContener;

	@FXML
	private TableColumn<Alergeno, Integer> columnContenrId;

	@FXML
	private TableColumn<Alergeno, String> columnContenerNombre;

	@FXML
	private TableView<Alergeno> tableNoContener;

	@FXML
	private TableColumn<Alergeno, Integer> columnNoContenerId;

	@FXML
	private TableColumn<Alergeno, String> columnNoContenerNombre;

	@FXML
	private TextArea textDescripcion;

	@FXML
	private TextField textNombre;

	@FXML
	private ComboBox<Marca> fieldMarca;

	@FXML
	void onActionAñadir(ActionEvent event) {
		if (Main.NAME_PANE.equals("ALERGENO")) {

			if (!textNombre.getText().equals("")) {
				Alergeno a = new Alergeno();
				a.setIdAlergeno(++Main.ALERGENO_MAX_ID);
				a.setNombre(textNombre.getText());
				a.setDescripcion(textDescripcion.getText());
				Main.BD_SESSION.addRow(a);
			} else {
				JOptionPane.showMessageDialog(null, "Nombre requerido", "Error", JOptionPane.WARNING_MESSAGE);
			}
		} else if (Main.NAME_PANE.equals("MARCA")) {

			if (!textNombre.getText().equals("")) {
				Marca a = new Marca();
				a.setIdMarca(++Main.MARCA_MAX_ID);
				a.setNombre(textNombre.getText());
				a.setDescripcion(textDescripcion.getText());
				Main.BD_SESSION.addRow(a);
			} else {
				JOptionPane.showMessageDialog(null, "Nombre requerido", "Error", JOptionPane.WARNING_MESSAGE);
			}
		} else {

			if (!textNombre.getText().equals("")) {
				Producto a = new Producto();
				a.setIdProducto(++Main.PRODUTO_MAX_ID);
				a.setMarca(fieldMarca.getValue());
				a.setDescripcion(textDescripcion.getText());
				a.setNombre(textNombre.getText());

				Set<Alergeno> listaAlergenos = new HashSet<>();

				for (Alergeno b : tableContener.getItems()) {
					listaAlergenos.add(b);
				}
				a.setUnion_alergenos(listaAlergenos);
				Main.BD_SESSION.addRow(a);
			} else {
				JOptionPane.showMessageDialog(null, "Nombre requerido", "Error", JOptionPane.WARNING_MESSAGE);
			}

		}
		Stage thisStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		thisStage.close();

	}

	@FXML
	void onActionCancelar(ActionEvent event) {
		Stage stage = (Stage) (((Node) event.getSource()).getScene().getWindow());
		stage.close();
	}

	@FXML
	void onActionAdd(ActionEvent event) {
		if (alergenoNoContener != null) {
			tableContener.getItems().add(alergenoNoContener);
			tableNoContener.getItems().remove(alergenoNoContener);
			alergenoNoContener = null;
		}
	}

	@FXML
	void onActionRemove(ActionEvent event) {
		if (alergenoContener != null) {
			tableNoContener.getItems().add(alergenoContener);
			tableContener.getItems().remove(alergenoContener);
			alergenoContener = null;
		}
	}

	@FXML
	void onClickedContener(MouseEvent event) {
		alergenoContener = tableContener.getSelectionModel().getSelectedItem();
	}

	@FXML
	void onClickedNoContener(MouseEvent event) {
		alergenoNoContener = tableNoContener.getSelectionModel().getSelectedItem();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.fxml.Initializable#initialize(java.net.URL,
	 * java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		paneTitle.setText(Main.NAME_PANE);
		switch (Main.NAME_PANE) {
		case "ALERGENO":

			// ReSize elementos
			textNombre.setPrefWidth(450);
			textDescripcion.setPrefHeight(190);
			// Ocultar Items no necesarios
			labelAlergenos.setVisible(false);
			tableContener.setVisible(false);
			tableNoContener.setVisible(false);
			labelMarca.setVisible(false);
			fieldMarca.setVisible(false);
			buttonAdd.setVisible(false);
			buttonRemove.setVisible(false);
			break;
		case "MARCA":
			// ReSize elementos
			textNombre.setPrefWidth(450);
			textDescripcion.setPrefHeight(190);
			// Ocultar Items no necesarios
			labelAlergenos.setVisible(false);
			tableContener.setVisible(false);
			tableNoContener.setVisible(false);
			labelMarca.setVisible(false);
			fieldMarca.setVisible(false);
			buttonAdd.setVisible(false);
			buttonRemove.setVisible(false);
			break;

		case "PRODUCTO":

			List<Marca> marcas = Main.BD_SESSION.getMarca();

			fieldMarca.getItems().addAll(marcas);
			fieldMarca.setValue(marcas.get(0));

			rellenarTablasAlergenos();

			break;

		default:
			JOptionPane.showMessageDialog(null, "1010: Pane desconocido", "Error", JOptionPane.WARNING_MESSAGE);
			System.exit(1);
			break;
		}

	}

	private void rellenarTablasAlergenos() {
		List<Alergeno> alergenos = Main.BD_SESSION.getAlergeno();

		ObservableList<Alergeno> obsAlerg = FXCollections.observableArrayList();
		ObservableList<Alergeno> obsAlerg2 = FXCollections.observableArrayList();
		Main.ALERGENO_MAX_ID = 0;
		for (Alergeno a : alergenos) {
			obsAlerg.add(a);
		}
		// Selecionr getter para la cada columna
		columnNoContenerId.setCellValueFactory(new PropertyValueFactory<>("IdAlergeno"));
		columnNoContenerNombre.setCellValueFactory(new PropertyValueFactory<>("Nombre"));

		columnContenrId.setCellValueFactory(new PropertyValueFactory<>("IdAlergeno"));
		columnContenerNombre.setCellValueFactory(new PropertyValueFactory<>("Nombre"));

		// aplicar la ObservableList en la tableview
		tableNoContener.setItems(obsAlerg);
		tableContener.setItems(obsAlerg2);
	}

}
