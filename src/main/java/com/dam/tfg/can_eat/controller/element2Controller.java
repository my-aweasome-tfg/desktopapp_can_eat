
package com.dam.tfg.can_eat.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import com.dam.tfg.can_eat.hibernate.modelo.Alergeno;
import com.dam.tfg.can_eat.hibernate.modelo.Marca;
import com.dam.tfg.can_eat.main.Main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class element2Controller implements Initializable {

	@FXML
	private ToolBar panePrincipal;

	@FXML
	private Button buttonCancelar;

	@FXML
	private Button buttonModif;

	@FXML
	private Text labelElemento;

	@FXML
	private TextField fieldId;

	@FXML
	private TextField fieldNombre;

	@FXML
	private TextArea fieldDescripcion;

	@FXML
	void onActionCancelar(ActionEvent event) {
		if (buttonCancelar.getText().equals("Cerrar")) {
//			menuController.loadTableAlergenos();
		
			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			stage.close();
		} else {
			setEditable(false);
			if (Main.NAME_PANE.equals("ALERGENO")) {
				try {
					show(Main.ALERGENO_SELECTED);
				} catch (SQLException e) {
					e.printStackTrace();
				}

			} else {
				try {
					show(Main.MARCA_SELECTED);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

	}

	@FXML
	void onActionModificar(ActionEvent event) {
		if (buttonModif.getText().equals("Modificar")) {
			setEditable(true);
		} else {
			setEditable(false);
			if (Main.NAME_PANE.equals("ALERGENO")) {
				
				if(!fieldNombre.getText().equals("")) {
					Alergeno a = new Alergeno();
					a.setIdAlergeno(Integer.parseInt(fieldId.getText()));
					a.setNombre(fieldNombre.getText());
					a.setDescripcion(fieldDescripcion.getText());
					
					System.out.println(fieldId.getText());
					System.out.println(a.toString());
					Main.BD_SESSION.updateRow(Integer.parseInt(fieldId.getText()), a);
				}else {
					JOptionPane.showMessageDialog(null, "Nombre requerido", "Error", JOptionPane.WARNING_MESSAGE);
				}
				

			} else {
				if(!fieldNombre.getText().equals("")) {
					Marca a = new Marca();
					a.setIdMarca(Integer.parseInt(fieldId.getText()));
					a.setNombre(fieldNombre.getText());
					a.setDescripcion(fieldDescripcion.getText());

					Main.BD_SESSION.updateRow(Integer.parseInt(fieldId.getText()), a);
				}else {
					JOptionPane.showMessageDialog(null, "Nombre requerido", "Error",JOptionPane.WARNING_MESSAGE);
				}
				
				

			}
			Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
			stage.close();

		}

	}

	private void setEditable(boolean bool) {
		if (bool) {
			buttonModif.setText("Guardar");
			buttonCancelar.setText("Cancelar");
		} else {
			buttonModif.setText("Modificar");
			buttonCancelar.setText("Cerrar");

		}
		fieldDescripcion.setEditable(bool);

		fieldNombre.setEditable(bool);

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		labelElemento.setText(Main.NAME_PANE);
		if (Main.NAME_PANE.equals("ALERGENO")) {
			try {
				show(Main.ALERGENO_SELECTED);
			} catch (SQLException e) {

				e.printStackTrace();
			}

		} else {
			try {
				show(Main.MARCA_SELECTED);
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

	}

	private void show(Marca a) throws SQLException {
		fieldId.setText(String.valueOf(a.getIdMarca()));
		// fieldMarca.setText(String.valueOf(a.getMarca().getIdMarca()));
		fieldNombre.setText(a.getNombre());
		fieldDescripcion.setText(a.getDescripcion());

	}

	private void show(Alergeno a) throws SQLException {
		fieldId.setText(String.valueOf(a.getIdAlergeno()));
		// fieldMarca.setText(String.valueOf(a.getMarca().getIdMarca()));
		fieldNombre.setText(a.getNombre());
		fieldDescripcion.setText(a.getDescripcion());

	}

}