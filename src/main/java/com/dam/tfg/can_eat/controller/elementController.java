package com.dam.tfg.can_eat.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javax.swing.JOptionPane;

import com.dam.tfg.can_eat.hibernate.modelo.Alergeno;
import com.dam.tfg.can_eat.hibernate.modelo.Marca;
import com.dam.tfg.can_eat.hibernate.modelo.Producto;
import com.dam.tfg.can_eat.jdbc.objects.Union_;
import com.dam.tfg.can_eat.main.Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class elementController implements Initializable {
	private Alergeno alergenoContener = null;
	private Alergeno alergenoNoContener = null;

	@FXML
	private BorderPane panePrincipal;

	@FXML
	private Button buttonCancelar;

	@FXML
	private Button buttonModifi;

	@FXML
	private Text labelElemento;

	@FXML
	private TextField fieldId;

	@FXML
	private TextField fieldNombre;

	@FXML
	private TextArea fieldDescripcion;

	@FXML
	private Text labelMarca;

	@FXML
	private ComboBox<Marca> fieldMarca;

	@FXML
	private TableView<Alergeno> tableAlegenos;

	@FXML
	private Text labelAlergeno;

	@FXML
	private TableColumn<Alergeno, Integer> colimnId;

	@FXML
	private TableColumn<Alergeno, String> columnNombre;

	@FXML
	private Button buttonAdd;

	@FXML
	private Button buttonRemove;

	@FXML
	void onActionCancelar(ActionEvent event) throws SQLException {
		if (buttonCancelar.getText().equals("Cerrar")) {

			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

			stage.close();

		} else {
			setEditable(false);
			setVisible(false);
			show(Main.PRODUTO_SELECTED);
		}

	}

	@FXML
	void onActionModificar(ActionEvent event) throws SQLException {
		if (buttonModifi.getText().equals("Modificar")) {
			setEditable(true);
			setVisible(true);
		} else {
			setEditable(false);
			setVisible(false);
			
			if(!fieldNombre.getText().equals("")) {
				Producto a = new Producto();
				a.setIdProducto(Integer.parseInt(fieldId.getText()));
				a.setNombre(fieldNombre.getText());
				a.setDescripcion(fieldDescripcion.getText());
				a.setMarca(fieldMarca.getValue());

				Set<Alergeno> alergenos = new HashSet<>();
				ObservableList<Alergeno> a2 = tableAlegenos.getItems();

				for (Alergeno alergeno : a2) {
					alergenos.add(alergeno);
				}
				a.setUnion_alergenos(alergenos);

				Main.BD_SESSION.updateRow(Integer.parseInt(fieldId.getText()), a);
				Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
				stage.close();
			}else {
				JOptionPane.showMessageDialog(null, "Nombre requerido", "Error", JOptionPane.WARNING_MESSAGE);
			}
			
		}

	}

	private void setEditable(boolean bool) {
		if (bool) {
			buttonModifi.setText("Guardar");
			buttonCancelar.setText("Cancelar");

		} else {
			buttonModifi.setText("Modificar");
			buttonCancelar.setText("Cerrar");

		}
		fieldDescripcion.setEditable(bool);
		fieldMarca.setDisable(!bool);
		fieldNombre.setEditable(bool);

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		labelElemento.setText(Main.NAME_PANE);
		try {
			show(Main.PRODUTO_SELECTED);
			setVisible(false);
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	public void setVisible(boolean bool) throws SQLException {
		buttonAdd.setVisible(bool);
		buttonRemove.setVisible(bool);
		tableNotAlergeno.setVisible(bool);

		if (bool) {
			tableAlegenos.setPrefWidth(200);

			List<Alergeno> alergenos = Main.BD_SESSION.getAlergeno();
			List<Union_> uniones = Main.JDBC_SESSION.getUnion_();

			ObservableList<Alergeno> obsAlerg2 = FXCollections.observableArrayList();

			for (Alergeno a : alergenos) {
				boolean valido = true;

				for (Union_ e : uniones) {
					if (e.getIdProducto() == Integer.parseInt(fieldId.getText())) {
						if (e.getIdAlergeno() == a.getIdAlergeno()) {
							valido = false;

						}
					}

				}
				if (valido) {
					System.out.println(a.getNombre());
					obsAlerg2.add(a);
				}
			}

			columnNotId.setCellValueFactory(new PropertyValueFactory<>("IdAlergeno"));
			columnNotName.setCellValueFactory(new PropertyValueFactory<>("Nombre"));

			tableNotAlergeno.setItems(obsAlerg2);

		} else {
			tableAlegenos.setPrefWidth(500);
		}
	}

	@FXML
	private TableView<Alergeno> tableNotAlergeno;

	@FXML
	private TableColumn<Alergeno, Integer> columnNotId;

	@FXML
	private TableColumn<Alergeno, String> columnNotName;

	@FXML
	void onActionAdd(ActionEvent event) {
		if (alergenoNoContener != null) {

			tableAlegenos.getItems().add(alergenoNoContener);
			tableNotAlergeno.getItems().remove(alergenoNoContener);

			alergenoNoContener = null;
		}
	}

	@FXML
	void onActionRemove(ActionEvent event) {
		if (alergenoContener != null) {

			tableNotAlergeno.getItems().add(alergenoContener);
			tableAlegenos.getItems().remove(alergenoContener);

			alergenoContener = null;
		}
	}

	private void show(Producto a) throws SQLException {
		fieldId.setText(String.valueOf(a.getIdProducto()));

		List<Marca> marcas = Main.BD_SESSION.getMarca();
		fieldMarca.getItems().addAll(marcas);
		int pos = getPos(marcas, a.getMarca());
		fieldMarca.setValue(marcas.get(pos));

		fieldNombre.setText(a.getNombre());
		fieldDescripcion.setText(a.getDescripcion());

		List<Alergeno> alergeno = Main.BD_SESSION.getAlergeno();

		List<Union_> uniones = Main.JDBC_SESSION.getUnion_();

		ObservableList<Alergeno> obsAlerg = FXCollections.observableArrayList();

		for (Union_ e : uniones) {
			if (e.getIdProducto() == a.getIdProducto()) {
				int idAlergeno = e.getIdAlergeno();
				for (Alergeno a2 : alergeno) {
					if (a2.getIdAlergeno() == idAlergeno) {
						obsAlerg.add(a2);
					}
				}

			}
		}

		colimnId.setCellValueFactory(new PropertyValueFactory<>("IdAlergeno"));
		columnNombre.setCellValueFactory(new PropertyValueFactory<>("Nombre"));

		tableAlegenos.setItems(obsAlerg);

	}

	private int getPos(List<Marca> marcas, Marca marca) {
		int i = 0;
		for (Marca marca2 : marcas) {
			if (marca2.getIdMarca() == marca.getIdMarca()) {
				return i;
			}
			i++;
		}

		return 0;
	}

	@FXML
	void onClickAlergeno(MouseEvent event) {
		alergenoContener = tableAlegenos.getSelectionModel().getSelectedItem();
	}

	@FXML
	void onCllckNotAlergeno(MouseEvent event) {
		alergenoNoContener = tableNotAlergeno.getSelectionModel().getSelectedItem();
	}

}
