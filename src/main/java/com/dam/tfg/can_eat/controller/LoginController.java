package com.dam.tfg.can_eat.controller;

import java.io.IOException;

import javax.swing.JOptionPane;

import com.dam.tfg.can_eat.hibernate.controller.HibernateSession;
import com.dam.tfg.can_eat.jdbc.Conector;
import com.dam.tfg.can_eat.main.Main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LoginController {
	@FXML
	private TextField textUser;

	@FXML
	private TextField textPassword;

	@FXML
	void OnEnterPassword(ActionEvent event) throws IOException {
		login(event);
	}

	@FXML
	void OnEnterUsuario(ActionEvent event) throws IOException {
		login(event);
	}

	@FXML
	void onActionLogin(ActionEvent event) throws IOException {
		login(event);

	}

	private void login(ActionEvent event) throws IOException {
		// Recuperar variables
		String user = textUser.getText();
		String pass = textPassword.getText();
		// Instanciar Cliente BD
		Main.BD_SESSION = new HibernateSession();

		if (Main.BD_SESSION.openSession(user, pass)) {
			// connexion jdbc uniones_
			try {
				Main.JDBC_SESSION = new Conector(user, pass);
			} catch (ClassNotFoundException e) {
				JOptionPane.showMessageDialog(null, "Fallo JDBC", "Login fail", JOptionPane.WARNING_MESSAGE);
			}
			switchStage(event);
		} else {
			JOptionPane.showMessageDialog(null, "Usuario o contraseña incorrecta", "Login fail",
					JOptionPane.WARNING_MESSAGE);
		}
	}

	private void switchStage(ActionEvent event) throws IOException {
		Parent blah = FXMLLoader.load(getClass().getResource("/view/menu.fxml"));
		Scene scene = new Scene(blah);
		Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		appStage.setScene(scene);
		appStage.show();
	}
}
