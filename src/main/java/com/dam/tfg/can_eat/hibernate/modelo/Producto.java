package com.dam.tfg.can_eat.hibernate.modelo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUCTO")
public class Producto {
	// Atributos
	@Id
	@Column(name = "ID")
	private int idProducto;

	@Column(name = "NOMBRE")
	private String nombre;

	@Column(name = "DESCRIPCION")
	private String descripcion;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDMARCA")
	private Marca marca;

	@ManyToMany
	@JoinTable(name = "UNION_", joinColumns = { @JoinColumn(name = "idProducto") }, inverseJoinColumns = {
			@JoinColumn(name = "idAlergeno") })
	private Set<Alergeno> union_alergenos = new HashSet<>();

	// Constructor
	public Producto() {
	}

	public Producto(int idProducto, String nombre, String descripcion) {
		this.idProducto = idProducto;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public Producto(int idProducto, String nombre, String descripcion, Marca marca) {
		this.idProducto = idProducto;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.marca = marca;
	}

	// Getters Setters

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		if (descripcion == null) {
			return "";
		}
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Set<Alergeno> getUnion_alergenos() {
		return union_alergenos;
	}

	public void setUnion_alergenos(Set<Alergeno> union_alergenos) {
		this.union_alergenos = union_alergenos;
	}

//	public void addAlergeno(Alergeno alergeno) {
//		this.union_alergenos.add(alergeno);
//	}
	@Override
	public String toString() {
		return "Producto [idProducto=" + idProducto + ", nombre=" + nombre + ", descripcion=" + descripcion + "]";
	}

}
