package com.dam.tfg.can_eat.hibernate.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.service.spi.ServiceException;

import com.dam.tfg.can_eat.hibernate.modelo.Alergeno;
import com.dam.tfg.can_eat.hibernate.modelo.Marca;
import com.dam.tfg.can_eat.hibernate.modelo.Producto;

public class HibernateSession {
	public EntityManager manager;
	public EntityManagerFactory emf;

	public HibernateSession() {
		this.manager = null;
		this.emf = null;
	}

	public boolean openSession(String user, String password) {
		boolean status = false;
		Map<String, String> properties = new HashMap<String, String>();

		properties.put("javax.persistence.jdbc.user", user);
		properties.put("javax.persistence.jdbc.password", password);
		try {
			this.emf = Persistence.createEntityManagerFactory("persistence", properties);
			status = true;
		} catch (ServiceException e) {
			status = false;
			System.out.println("Fail login");
		}
		return status;
	}

	public List<Alergeno> getAlergeno() {
		List<Alergeno> list = new ArrayList<Alergeno>();
		this.manager = this.emf.createEntityManager();
		@SuppressWarnings("unchecked")
		List<Alergeno> alergenos = (List<Alergeno>) 
			this.manager.createQuery("FROM Alergeno").getResultList();
		for (Alergeno a : alergenos) {
			list.add(a);
		}
		this.manager.close();
		return list;
	}

	public List<Marca> getMarca() {
		List<Marca> list = new ArrayList<Marca>();

		this.manager = this.emf.createEntityManager();
		@SuppressWarnings("unchecked")
		List<Marca> marcas = (List<Marca>) this.manager.createQuery("FROM Marca").getResultList();
		for (Marca a : marcas) {
			list.add(a);
		}
		this.manager.close();

		return list;
	}

	public List<Producto> getProducto() {
		List<Producto> list = new ArrayList<Producto>();

		this.manager = this.emf.createEntityManager();
		@SuppressWarnings("unchecked")
		List<Producto> productos = (List<Producto>) this.manager.createQuery("FROM Producto").getResultList();
		for (Producto a : productos) {
			list.add(a);
		}
		this.manager.close();

		return list;
	}

	public void Close() {
		this.emf.close();
	}

	public Boolean addRow(Marca a) {
		try {

			this.manager = this.emf.createEntityManager();

			this.manager.getTransaction().begin();

			this.manager.persist(a);

			this.manager.getTransaction().commit();

			this.manager.close();

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	public Boolean addRow(Alergeno a) {
		try {

			this.manager = this.emf.createEntityManager();

			this.manager.getTransaction().begin();

			this.manager.persist(a);

			this.manager.getTransaction().commit();

			this.manager.close();

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	public Boolean addRow(Producto a) {
		try {

			this.manager = this.emf.createEntityManager();
			this.manager.getTransaction().begin();
			this.manager.persist(a);
			this.manager.getTransaction().commit();
			this.manager.close();

		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public boolean deleteRow(Marca a) {
		try {

			this.manager = this.emf.createEntityManager();

			this.manager.getTransaction().begin();

			a = this.manager.find(Marca.class, a.getIdMarca());
			this.manager.remove(a);
			this.manager.getTransaction().commit();

			this.manager.getTransaction().commit();

			this.manager.close();

		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public boolean deleteRow(Alergeno a) {
		try {
			this.manager = this.emf.createEntityManager();
			this.manager.getTransaction().begin();
			a = this.manager.find(Alergeno.class, a.getIdAlergeno());
			this.manager.remove(a);
			this.manager.getTransaction().commit();
			this.manager.getTransaction().commit();
			this.manager.close();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public boolean deleteRow(Producto a) {
		try {

			this.manager = this.emf.createEntityManager();

			this.manager.getTransaction().begin();

			a = this.manager.find(Producto.class, a.getIdProducto());
			this.manager.remove(a);
			this.manager.getTransaction().commit();

			this.manager.close();

		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public boolean updateRow(int id, Producto b) {
		try {

			this.manager = this.emf.createEntityManager();

			Producto a = manager.find(Producto.class, id);

			this.manager.getTransaction().begin();

			a.setIdProducto(b.getIdProducto());
			a.setNombre(b.getNombre());
			a.setDescripcion(b.getDescripcion());
			a.setMarca(b.getMarca());
			a.setUnion_alergenos(b.getUnion_alergenos());

			this.manager.getTransaction().commit();

			this.manager.close();

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	public boolean updateRow(int id, Alergeno b) {
		try {
			this.manager = this.emf.createEntityManager();
			Alergeno a = manager.find(Alergeno.class, id);
			this.manager.getTransaction().begin();
			a.setIdAlergeno(b.getIdAlergeno());
			a.setNombre(b.getNombre());
			a.setDescripcion(b.getDescripcion());
			a.setUnion_productos(b.getUnion_productos());
			this.manager.getTransaction().commit();
			this.manager.close();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public boolean updateRow(int id, Marca b) {
		try {

			this.manager = this.emf.createEntityManager();

			Marca a = manager.find(Marca.class, id);

			this.manager.getTransaction().begin();

			a.setIdMarca(b.getIdMarca());
			a.setNombre(b.getNombre());
			a.setDescripcion(b.getDescripcion());

			this.manager.getTransaction().commit();

			this.manager.close();

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	public Marca selectAMarca(Integer id) {

		this.manager = this.emf.createEntityManager();

		Marca a = manager.find(Marca.class, id);

		this.manager.close();

		return a;
	}

	public Alergeno selectAAlergen(Integer id) {

		this.manager = this.emf.createEntityManager();

		Alergeno a = manager.find(Alergeno.class, id);

		this.manager.close();

		return a;
	}

}
