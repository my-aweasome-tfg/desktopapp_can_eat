package com.dam.tfg.can_eat.hibernate.modelo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ALERGENO")
public class Alergeno {
	// Atributos
	@Id
	@Column(name = "ID")
	private int idAlergeno;
	@Column(name = "NOMBRE")
	private String nombre;
	@Column(name = "DESCRIPCION")
	private String descripcion;

	@ManyToMany(cascade = { CascadeType.REMOVE }, mappedBy = "union_alergenos")
	private Set<Producto> union_productos = new HashSet<>();

	// Constructores
	public Alergeno() {
	}

	public Alergeno(int idAlergeno, String nombre, String descripcion) {
		this.idAlergeno = idAlergeno;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	// GetterSetter

	public int getIdAlergeno() {
		return idAlergeno;
	}

	public void setIdAlergeno(int idAlergeno) {
		this.idAlergeno = idAlergeno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		if(descripcion==null){
			return "";
		}
		return descripcion;
	}
	

	public Set<Producto> getUnion_productos() {
		return union_productos;
	}

	public void setUnion_productos(Set<Producto> union_productos) {
		this.union_productos = union_productos;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	

	public Set<Producto> getProductos() {
		return union_productos;
	}

	public void setProductos(Set<Producto> productos) {
		this.union_productos = productos;
	}

	@Override
	public String toString() {
		return "Alergeno [idAlergeno=" + idAlergeno + ", nombre=" + nombre + ", descripcion=" + descripcion + "]";
	}

}
