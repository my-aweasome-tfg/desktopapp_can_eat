package com.dam.tfg.can_eat.hibernate.modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "MARCA")
public class Marca {
	@Id
	@Column(name = "ID")
	private int idMarca;
	@Column(name = "NOMBRE")
	private String nombre;
	@Column(name = "DESCRIPCION")
	private String descripcion;
	
	@OneToMany(mappedBy = "marca", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Producto> productos = new ArrayList<Producto>();
	
	
	
	public Marca() {
	}

	public Marca(int idMarca, String nombre, String descripcion) {
		this.idMarca = idMarca;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public int getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(int idMarca) {
		this.idMarca = idMarca;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		if(descripcion==null){
			return "";
		}
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public void addProduct(Producto p) {
		this.productos.add(p);
	}

	@Override
	public String toString() {
		return this.nombre;
	}
	
	    
}
