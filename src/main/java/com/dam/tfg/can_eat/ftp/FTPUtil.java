package com.dam.tfg.can_eat.ftp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.dam.tfg.can_eat.jdom.XMLMaker;

public class FTPUtil {

	public static void sendFile() {
		String server = "files.000webhost.com";
		int port = 21;
		String user = "canIEatIt";
		String pass = "alumnealumne";
		FtpSynchronizer not = new FtpSynchronizer();
		ClientFtpProtocolService protocol = new ClientFtpProtocolService();

		protocol.init(not, System.out);
		try {
			protocol.connectTo(server, port);
			protocol.authenticate(user, pass);

			System.out.println(protocol.sendPassv());
			System.out.println(protocol.sendList(System.out, false));
			System.out.println(protocol.sendPwd());
			System.out.println(protocol.sendCwd("public_html"));

			System.out.println(protocol.sendPassv());
			System.out.println(protocol.sendList(System.out, false));

			File f = new File("PlantillaXML2.xml");
			BufferedWriter bffw = new BufferedWriter(new FileWriter(f));
			bffw.write(XMLMaker.make());
			bffw.close();
			System.out.println(protocol.sendPassv());
			System.out.println(protocol.sendStor("/public_html/" + f.getName(), // Podem especificar camí absolut
					new FileInputStream(f.getAbsolutePath()), true));

			f.delete();

			System.out.println(protocol.sendQuit());
		} catch (IOException | ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(null, "Fallo FTP", "Error", JOptionPane.WARNING_MESSAGE);
			e.printStackTrace();
		}
	}
}
