package com.dam.tfg.can_eat.ftp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Josep Cañellas <jcanell4@ioc.cat>
 */
public class ClientFtpProtocolService implements Runnable{
    public static final String USER = "USER ";
    public static final String PASS = "PASS ";
    public static final String PASV = "PASV";
    public static final String LIST = "LIST";
    public static final String RETR = "RETR ";
    public static final String CDUP = "CDUP";
    public static final String PORT = "PORT ";
    public static final String CWD = "CWD ";
    public static final String PWD = "PWD";
    public static final String QUIT = "QUIT";
    public static final String STOR = "STOR ";
    
    private FtpSynchronizer notifier;
    private Socket controlChannelSocket;
    private Socket datatChannelSocket=null;
    private BufferedReader in=null;
    private PrintWriter out=null;
    private boolean end=false;
    private PrintWriter systemOutput;
    private boolean passiveError=false;

//    private ClientFtpControlService ftpClient;
    
    public void init(FtpSynchronizer notifier, OutputStream  systemOutput){
        this.systemOutput = new PrintWriter(systemOutput);
        this.notifier=notifier;
    }
    
    public void connectTo(String server, int port) throws IOException{
        InetAddress inetAddressServer = InetAddress.getByName(server);
        controlChannelSocket = new Socket(inetAddressServer, port);
        in = new BufferedReader(new InputStreamReader(controlChannelSocket.getInputStream()));
        out = new PrintWriter(controlChannelSocket.getOutputStream());

        (new Thread(this)).start();
    }
    
    public void authenticate(String user, String pass) {
        sendUser(user);
        sendPass(pass);
    }
    

    public String sendPassv(){
        String ret = PASV;
        notifier.disableSynchronizer();
        send(ret);
        return ret;
    }

    public String sendList(OutputStream out, boolean closeOutput) 
                                                        throws IOException{
        String ret = LIST;
        send(ret);
        processInputData(out, closeOutput);
        return ret;
    }

    public String sendCdup(){
        String ret = CDUP;
        send(ret);
        return ret;
    }
    
    public String sendCwd(String down){
        String ret = CWD + down;
        send(ret);
        return ret;
    }
    
    public String sendPwd(){
        String ret = PWD;
        send(ret);
        return ret;
    }
    
    public String sendQuit(){
        String ret = QUIT;
        send(ret);
        return ret;
    }
    
    // Funció per rebre un arxiu
    public String sendRetr(String remote, OutputStream out, boolean closeOutput) 
                                                            throws IOException{
        String ret = RETR + remote;
        send(ret);
        processInputData(out, closeOutput);
        return ret;
    }
    
    // Afegit: Funció per enviar un arxiu
    public String sendStor(String remote, InputStream in, boolean closeOutput) 
            throws IOException{
    		String ret = STOR + remote;
    		send(ret);
    		processInputData(in, closeOutput);
    		return ret;
    }
    public String sendUser(String user){
        String ret = USER + user;
        send(ret);
        return ret;
    }
    
    public String sendPass(String pass){
        String ret = PASS + pass;
        send(ret);
        return ret;
    }
    
    public void close(){
        if(!controlChannelSocket.isClosed()){
            sendQuit();
        }
        //close(controlChannelSocket);
        //close(datatChannelSocket);            
    }
    
    private void createDataSocket(byte[] addres, int port){
        try {
            if(notifier.isEnabled()){
                notifier.disableSynchronizer();
            }

            InetAddress inetAddressServer = InetAddress.getByAddress(addres);
            datatChannelSocket = new Socket(inetAddressServer, port);
            passiveError=false;
            
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            passiveError=true;
        }
        
        notifier.youCanStart();            
    }
    
    private void processResponse(String ret) throws IOException{
        if(ret.startsWith("227")){
            //init passive mode
            passiveService(ret.substring(ret.indexOf('(')+1,ret.indexOf(')')));
        }
    }
    
    private void passiveService(String str) throws IOException{
        byte[] adress = new byte[4];
        String[] strBytes = str.split(",");
        for(int i=0; i<4; i++){
            adress[i]=(byte) Integer.parseInt(strBytes[i]);
        }
        int port = Integer.parseInt(strBytes[4])*256;
        port += Integer.parseInt(strBytes[5]);
        
        createDataSocket(adress, port);
    }
    
    private void processInputData(OutputStream out, boolean closeOutput) 
                                                            throws IOException{
        if(notifier.isDisabled()){
            notifier.waitingToStart();
        }else{
            //Error  cal PASV
            return;
        }

        if(!passiveError){
            ClientFtpDataService channel = new ClientFtpDataService();

            channel.init(notifier, datatChannelSocket, out, closeOutput);
            (new Thread(channel)).start();
        }
    }
    
    private void processInputData(InputStream in, boolean closeOutput) 
            throws IOException{
    	if(notifier.isDisabled()){
    		notifier.waitingToStart();
    	}else{
    		//Error  cal PASV
    		return;
    	}

    	if(!passiveError){
    		ClientFtpDataService channel = new ClientFtpDataService();

    		channel.init(notifier, datatChannelSocket, in, closeOutput);
    		(new Thread(channel)).start();
    	}
    }

        
    /**
     * Executa el procés d'escoltar al servidor en un fil diferent.
     */
    @Override
    public void run(){
        try {
            listen();
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }finally{
            close(controlChannelSocket);
            close(datatChannelSocket);            
        }

    }
    
    /**
     * Escolta el servidor fins que l'usuari indiqui el tancament i el servidor 
     * mantingui oberta la connexió.
     */
    private void listen() throws IOException{
        String inputLine = readLine();
        end = inputLine==null;
        while(!end){                
            processResponse(inputLine);
            inputLine = readLine();
            end = inputLine==null;
        }
    }

    private void send(String message){
        out.println(message);
        out.flush();
    }
    
    private String readLine() throws IOException{
        String ret = in.readLine();
        if(ret!=null){
            systemOutput.println(ret);
            systemOutput.flush();
        }
        return ret;
    }
    

    
    private void close(Socket socket){
        if(socket!=null && !socket.isClosed()){
            if(!socket.isInputShutdown()){
                try {
                    socket.shutdownInput();
                } catch (IOException ex) {
                    Logger.getLogger(ClientFtpProtocolService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if(!socket.isOutputShutdown()){
                try {
                    socket.shutdownOutput();
                } catch (IOException ex) {
                    Logger.getLogger(ClientFtpProtocolService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                socket.close();
            } catch (IOException ex) {
                Logger.getLogger(ClientFtpProtocolService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
