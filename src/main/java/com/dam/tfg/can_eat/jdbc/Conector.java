package com.dam.tfg.can_eat.jdbc;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.dam.tfg.can_eat.jdbc.objects.Union_;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

//TODO Eliminar Clase Conector si no se utilizara
public class Conector {
	private String url = "jdbc:mysql://localhost:3306/";
	public Connection connection;

	public Connection getConnection() {
		return connection;
	}

	public Conector(String user, String pass) throws ClassNotFoundException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.connection = (Connection) DriverManager.getConnection(url + "can_eat", user, pass);
			if (connection != null) {
				System.out.println("Conectado");
			}
		} catch (SQLException ex) {
			System.err.println("No se ha podido conectar a la base de datos\n" + ex.getMessage());
		}
	}
	
	

	public void close() {
		try {
			connection.close();
		} catch (SQLException ex) {
			Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public List<Union_> getUnion_() throws SQLException{
		List<Union_> list = new ArrayList<>();
		Statement st = (Statement) connection.createStatement();
		ResultSet rs = st.executeQuery("select * from union_");
		
		
		while(rs.next()) {
			list.add(new Union_(rs.getInt("idproducto"), rs.getInt("idalergeno")));
		}
		rs.close();
		st.close();
				
		return list;
	}

}