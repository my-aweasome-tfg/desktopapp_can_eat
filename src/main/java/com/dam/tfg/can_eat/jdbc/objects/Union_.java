package com.dam.tfg.can_eat.jdbc.objects;

public class Union_ {
	private int idProducto;
	private int idAlergeno;

	public Union_(int idProducto, int idAlergeno) {
		this.idProducto = idProducto;
		this.idAlergeno = idAlergeno;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public int getIdAlergeno() {
		return idAlergeno;
	}

	public void setIdAlergeno(int idAlergeno) {
		this.idAlergeno = idAlergeno;
	}

	@Override
	public String toString() {
		return "Union_ [idProducto=" + idProducto + ", idAlergeno=" + idAlergeno + "]";
	}
//	
}
