package com.dam.tfg.can_eat.jdom;

import java.sql.SQLException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.dam.tfg.can_eat.hibernate.controller.HibernateSession;
import com.dam.tfg.can_eat.hibernate.modelo.Alergeno;
import com.dam.tfg.can_eat.hibernate.modelo.Marca;
import com.dam.tfg.can_eat.hibernate.modelo.Producto;
import com.dam.tfg.can_eat.jdbc.Conector;
import com.dam.tfg.can_eat.jdbc.objects.Union_;
import com.dam.tfg.can_eat.main.Main;

public class XMLMaker {
	
	public static String make() throws ClassNotFoundException, SQLException {

		Element root = new Element("root");
		Document doc = new Document(root);

		Element marcas = new Element("marcas");
		root.addContent(marcas);

		List<Marca> listMarcas = Main.BD_SESSION.getMarca();

		for (Marca e : listMarcas) {
			Element marca = new Element("marca");

			Element id = new Element("id");
			id.addContent(new String(String.valueOf(e.getIdMarca())));
			marca.addContent(id);

			Element nombre = new Element("nombre");
			nombre.addContent(new String(e.getNombre()));
			marca.addContent(nombre);

//			Element descripcion = new Element("descripcion");
//			descripcion.addContent(new String(e.getDescripcion()));
//			marca.addContent(descripcion);

			marcas.addContent(marca);
		}

		Element alergenos = new Element("alergenos");
		root.addContent(alergenos);

		List<Alergeno> listAlergenos = Main.BD_SESSION.getAlergeno();
		for (Alergeno e : listAlergenos) {
			Element alergeno = new Element("alergeno");

			Element id = new Element("id");
			id.addContent(new String(String.valueOf(e.getIdAlergeno())));
			alergeno.addContent(id);

			Element nombre = new Element("nombre");
			nombre.addContent(new String(e.getNombre()));
			alergeno.addContent(nombre);

			Element descripcion = new Element("descripcion");
			descripcion.addContent(new String(e.getDescripcion()));
			alergeno.addContent(descripcion);

			Element img = new Element("img");
			img.addContent(new String(e.getNombre() + ".png"));
			alergeno.addContent(img);

			alergenos.addContent(alergeno);
		}

		Element productos = new Element("productos");
		root.addContent(productos);

		List<Producto> listProductos = Main.BD_SESSION.getProducto();
		for (Producto e : listProductos) {
			Element producto = new Element("producto");

			Element id = new Element("id");
			id.addContent(new String(String.valueOf(e.getIdProducto())));
			producto.addContent(id);

			Element idMarca = new Element("idMarca");
			idMarca.addContent(new String(String.valueOf(e.getMarca().getIdMarca())));
			producto.addContent(idMarca);

			Element nombre = new Element("nombre");
			nombre.addContent(new String(e.getNombre()));
			producto.addContent(nombre);

			Element descripcion = new Element("descripcion");
			descripcion.addContent(new String(e.getDescripcion()));
			producto.addContent(descripcion);

			productos.addContent(producto);
		}

		Element uniones = new Element("union_alerg");
		root.addContent(uniones);

		List<Union_> listaUnion = Main.JDBC_SESSION.getUnion_();

		for (Union_ e : listaUnion) {

			Element union = new Element("union");

			Element idProducto = new Element("id_product");
			idProducto.addContent(new String(String.valueOf(e.getIdProducto())));
			union.addContent(idProducto);

			Element idAlergeno = new Element("id_alerg");
			idAlergeno.addContent(new String(String.valueOf(e.getIdAlergeno())));
			union.addContent(idAlergeno);

			uniones.addContent(union);

		}

		XMLOutputter xmlOutput = new XMLOutputter();

		xmlOutput.setFormat(Format.getPrettyFormat().setOmitDeclaration(true));

		String xml = xmlOutput.outputString(doc);
		return xml;
	}
}
